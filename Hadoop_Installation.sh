sudo apt-get update

#install the java in ubuntu

sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
sudo apt install -y oracle-java8-installer

#set environment variable for Hadoop enviroment to find (Not Madatory)
echo "export JAVA_HOME=/usr" >> /etc/profile
source /etc/profile

#Download Hadoop setup from the 
sudo wget http://apache.claz.org/hadoop/common/hadoop-3.1.1/hadoop-3.1.1.tar.gz
sudo tar -xzvf hadoop-3.1.1.tar.gz
mv hadoop-3.1.1 /usr/local/hadoop
